import lejos.hardware.Sound
import java.io.File

/**
 * Time tracker to track total driving and calculating time
 *
 * @constructor Create new Time tracker
 */
class TimeTracker {
    var totalTime = 0L
    private var currentTimestamp = 0L

    /**
     * Start timer and play a sound
     *
     */
    fun startTimings() {
        Sound.setVolume(Sound.VOL_MAX)
        Sound.beepSequenceUp()
        currentTimestamp = System.currentTimeMillis()
    }

    /**
     * Stop timer and play a sound
     *
     */
    fun stopTimings() {
        if (currentTimestamp == 0L)
            return
        Sound.beepSequence()
        totalTime += System.currentTimeMillis() - currentTimestamp
        currentTimestamp = 0L
    }
}

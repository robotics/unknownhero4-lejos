package navigation

import model.Node
import java.io.File

/**
 * Main
 *
 */
fun main() {
    val maze = MazeReader("src/main/resources/asciimap")
    maze.printMaze()
}

/**
 * Maze reader
 *
 * This class reads an given maze file in Ascii format and parses it to a node network.
 *
 * @constructor
 *
 * @param path to the file with the maze
 */
class MazeReader(path: String) {
    // Stores start node
    lateinit var start: Node

    // Stores goal node
    lateinit var goal: Node

    // Stores nodes in a 2D-Array
    val nodes: MutableList<MutableList<Node?>> = mutableListOf()

    init {
        // Reads maze file and creates nodes on the given positions
        val maze = File(path).readLines()
        maze.forEachIndexed { y, line ->
            val row = mutableListOf<Node?>()
            line.forEachIndexed { x, cell ->
                if (cell == ' ') {
                    row.add(null)
                } else {
                    val node = Node(x, y)
                    row.add(node)
                    if (cell == 'G') {
                        goal = node
                    } else if (cell == 'S') {
                        start = node
                    }
                }
            }
            nodes.add(row)
        }
        // Sets the connections to the neighbouring nodes
        nodes.forEachIndexed { y, row ->
            row.forEachIndexed { x, node ->
                node?.let {
                    getNode(x, y - 1)?.let { node.up = getNode(x, y - 2) ?: Node(x, y - 2) }
                    getNode(x, y + 1)?.let { node.down = getNode(x, y + 2) ?: Node(x, y + 2) }
                    getNode(x - 1, y)?.let { node.left = getNode(x - 2, y) ?: Node(x - 2, y) }
                    getNode(x + 1, y)?.let { node.right = getNode(x + 2, y) ?: Node(x + 2, y) }
                }
            }
        }
    }

    /**
     * Get node
     *
     * @param x position
     * @param y position
     * @return node on given position or null if invalid
     */
    fun getNode(x: Int, y: Int): Node? {
        return try {
            nodes[y][x]
        } catch (e: IndexOutOfBoundsException) {
            null
        }
    }

    /**
     * Print maze
     *
     */
    fun printMaze() {
        nodes.forEach { row ->
            row.forEach { node ->
                when (node) {
                    goal -> {
                        print("G")
                    }
                    start -> {
                        print("S")
                    }
                    null -> {
                        print(" ")
                    }
                    else -> {
                        print("*")
                    }
                }
            }
            println()
        }
    }
}

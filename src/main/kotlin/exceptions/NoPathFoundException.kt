package exceptions

class NoPathFoundException(override val message: String? = null) : Exception(message)

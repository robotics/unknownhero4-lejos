package exceptions

class SensorIndexException(override val message: String? = null) : Exception(message)

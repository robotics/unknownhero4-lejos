package exceptions

import java.lang.Exception

class UnexpectedDirectionException(override val message: String? = null) : Exception(message)

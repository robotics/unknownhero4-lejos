package exceptions

import java.lang.Exception

class UnexpectedSensorValueException(override val message: String? = null) : Exception(message)

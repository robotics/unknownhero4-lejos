package model

import exceptions.UnexpectedActionException

enum class Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT;

    fun turnLeft(): Direction {
        return when (this) {
            UP -> LEFT
            RIGHT -> UP
            DOWN -> RIGHT
            LEFT -> DOWN
        }
    }

    fun turnRight(): Direction {
        return when (this) {
            UP -> RIGHT
            RIGHT -> DOWN
            DOWN -> LEFT
            LEFT -> UP
        }
    }

    companion object {
        fun derivative(from: Direction, to: Direction): Array<Move> {
            return when (Pair(from, to)) {
                Pair(UP, UP), Pair(RIGHT, RIGHT), Pair(DOWN, DOWN), Pair(LEFT, LEFT) ->
                    arrayOf(Move.NEXT_POSITION)

                Pair(UP, RIGHT), Pair(RIGHT, DOWN), Pair(DOWN, LEFT), Pair(LEFT, UP) ->
                    arrayOf(Move.TURN_RIGHT, Move.NEXT_POSITION)

                Pair(UP, LEFT), Pair(LEFT, DOWN), Pair(DOWN, RIGHT), Pair(RIGHT, UP) ->
                    arrayOf(Move.TURN_LEFT, Move.NEXT_POSITION)
                else -> throw UnexpectedActionException("Could not derive Move from ${Pair(from, to)}")
            }
        }
    }
}

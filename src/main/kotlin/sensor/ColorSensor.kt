package sensor

import lejos.hardware.port.SensorPort
import lejos.hardware.sensor.EV3ColorSensor

/**
 * Color sensor
 *
 * @constructor Create new Color sensor
 */
class ColorSensor {
    private val sensor = EV3ColorSensor(SensorPort.S3)

    /**
     * Get r g b values from the color sensor
     *
     * @return triple with r g b values
     */
    fun getRGB(): Triple<Float, Float, Float> {
        val sample = FloatArray(3)
        sensor.rgbMode.fetchSample(sample, 0)
        return Triple(sample[0], sample[1], sample[2])
    }
}

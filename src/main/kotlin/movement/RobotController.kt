package movement

import config.Config
import config.Config.BLOCK_PUSH_DISTANCE
import config.Config.BLOCK_PUSH_SPEED_ANGULAR_PERCENT
import config.Config.BLOCK_PUSH_SPEED_PERCENT
import config.Config.JUNCTION_IGNORE_DISTANCE
import config.Config.SPEED_ANGULAR_PERCENT
import config.Config.SPEED_PERCENT
import config.Config.WHEEL_DIAMETER
import config.Config.WHEEL_OFFSET_LEFT
import config.Config.WHEEL_OFFSET_RIGHT
import config.Config.WHEEL_PORT_LEFT
import config.Config.WHEEL_PORT_RIGHT
import exceptions.UnexpectedStatusException
import lejos.hardware.BrickFinder
import lejos.hardware.motor.EV3LargeRegulatedMotor
import lejos.robotics.chassis.Wheel
import lejos.robotics.chassis.WheeledChassis
import model.Status
import sensor.ColorSensor
import sensor.LightArraySensor
import sensor.LightArraySensor.Companion.verify
import sensor.UltrasonicSensor
import kotlin.math.abs

/**
 * Robot controller is responsible to communicate with the sensors and motors to view its surrounding and react properly.
 * The robot controller gets it's tasks from the controller.
 *
 * @constructor Create empty Robot controller
 */
class RobotController {
    private val pidController = PIDController()
    private val brick = BrickFinder.getLocal()
    private val lcd = brick.textLCD

    // Chassis
    private val wheel1: Wheel =
        WheeledChassis.modelWheel(
            EV3LargeRegulatedMotor(BrickFinder.getDefault().getPort(WHEEL_PORT_LEFT)),
            WHEEL_DIAMETER
        ).offset(WHEEL_OFFSET_LEFT)
    private val wheel2: Wheel =
        WheeledChassis.modelWheel(
            EV3LargeRegulatedMotor(BrickFinder.getDefault().getPort(WHEEL_PORT_RIGHT)),
            WHEEL_DIAMETER
        ).offset(WHEEL_OFFSET_RIGHT)
    private val chassis = WheeledChassis(arrayOf(wheel1, wheel2), WheeledChassis.TYPE_DIFFERENTIAL)

    // Sensors
    private val lineLeader = LightArraySensor()
    private val ultrasonicSensor = UltrasonicSensor()
    private val colorSensor = ColorSensor()

    private val speed = SPEED_PERCENT * chassis.maxLinearSpeed
    private val angularSpeed = SPEED_ANGULAR_PERCENT * chassis.maxAngularSpeed

    init {
        chassis.angularSpeed = angularSpeed
        chassis.linearSpeed = speed
    }

    /**
     * Drives to next position with given parameters
     *
     * @param forward boolean: true -> drive forward, false -> drive backwards
     * @param detectBlock boolean: true -> detect blocks on the way, false -> dont detect blocks
     * @param ignoreStartingJunction boolean: true -> ignores the first detected junctions on a given distance, false -> dont ignore
     * @param distance double or null: double -> max distance to drive or else stop, null -> drive to next junction
     * @return status blue/red block detected or success
     */
    fun driveToPosition(
        forward: Boolean,
        detectBlock: Boolean,
        ignoreStartingJunction: Boolean,
        ignoreLineLost: Boolean,
        distance: Double? = null,
    ): Status {
        wheel1.motor.resetTachoCount()
        wheel2.motor.resetTachoCount()

        while (true) {
            val values = lineLeader.inputValues
            when (verify(values)) {
                Status.SUCCESS -> {
                    if (detectBlock && ultrasonicSensor.getDistance() < Config.BLOCK_DETECTION_DISTANCE) {
                        stop()
                        val (red, _, blue) = colorSensor.getRGB()
                        return if (red > blue) Status.RED_BLOCK_ENCOUNTERED else Status.BLUE_BLOCK_ENCOUNTERED
                    }

                    if (distance != null && abs(wheel1.motor.tachoCount + wheel2.motor.tachoCount) / 2 >= distance) {
                        stop()
                        return Status.SUCCESS
                    }
                    if (forward) {
                        chassis.setVelocity(speed, pidController.getAngle(values))
                    } else {
                        chassis.setVelocity(-speed, pidController.getAngle(values))
                    }
                }
                Status.JUNCTION_ENCOUNTERED -> {
                    if (!ignoreStartingJunction || abs(wheel1.motor.tachoCount + wheel2.motor.tachoCount) / 2 >= JUNCTION_IGNORE_DISTANCE) {
                        stop()
                        return Status.SUCCESS
                    }
                }
                Status.LINE_LOST -> {
                    if (!ignoreLineLost) {
                        if (forward) chassis.setVelocity(-speed, 0.0) else chassis.setVelocity(speed, 0.0)
                    } else {
                        chassis.setVelocity(speed, 0.0)
                        if (forward) chassis.setVelocity(speed, 0.0) else chassis.setVelocity(-speed, 0.0)
                    }
                }
                else -> throw UnexpectedStatusException()
            }
        }
    }

    /**
     * Push block to the next junction
     *
     */
    fun pushBlock() {
        chassis.angularSpeed = BLOCK_PUSH_SPEED_ANGULAR_PERCENT * chassis.maxAngularSpeed
        chassis.linearSpeed = BLOCK_PUSH_SPEED_PERCENT * chassis.maxLinearSpeed

        driveToPosition(
            forward = true,
            detectBlock = false,
            ignoreStartingJunction = false,
            ignoreLineLost = false
        )
        driveToPosition(
            forward = true,
            detectBlock = false,
            ignoreStartingJunction = true,
            ignoreLineLost = true,
            distance = BLOCK_PUSH_DISTANCE
        )

        chassis.angularSpeed = angularSpeed
        chassis.linearSpeed = speed

        driveToPosition(
            forward = false,
            detectBlock = false,
            ignoreStartingJunction = false,
            ignoreLineLost = false
        )
        driveToPosition(
            forward = true,
            detectBlock = false,
            ignoreStartingJunction = false,
            ignoreLineLost = true
        )
    }

    /**
     * Turn left
     *
     */
    fun turnLeft() {
        stop()
        chassis.rotate(-90.0)
        while (chassis.isMoving)
            Thread.yield()
    }

    /**
     * Turn right
     *
     */
    fun turnRight() {
        stop()
        chassis.rotate(90.0)
        while (chassis.isMoving)
            Thread.yield()
    }

    fun turnAround() {
        stop()
        chassis.rotate(180.0)
        while (chassis.isMoving)
            Thread.yield()
    }

    /**
     * Print text to screen
     *
     * @param text to be printed
     */
    fun printToScreen(text: String, clear: Boolean = true) {
        if (clear) {
            lcd.clear()
            lcd.scroll()
            lcd.scroll()
            lcd.scroll()
            lcd.scroll()
            lcd.scroll()
        }
        println(text)
    }

    /**
     * Calibrate the light array sensor
     *
     */
    fun calibrate() =
        lineLeader.calibrate(brick)

    /**
     * Wait for button press
     *
     */
    fun waitForButtonPress() =
        brick.getKey("Enter").waitForPressAndRelease()

    /**
     * Stop and reset the pid controller
     *
     */
    fun stop() {
        pidController.reset()
        if (chassis.isMoving) {
            chassis.stop()
            chassis.waitComplete()
        }
    }
}

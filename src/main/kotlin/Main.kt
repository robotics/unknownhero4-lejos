/**
 * Main starts the [Controller] and therefore the whole program
 *
 */
fun main() {
    val controller = Controller()
    controller.start()
}

object TestMaps {
    val maps = listOf(
        TestMap("src/test/resources/maps/map01", true),
        TestMap("src/test/resources/maps/map02", true),
        TestMap("src/test/resources/maps/map03", true),
        TestMap("src/test/resources/maps/map04", true),
        TestMap("src/test/resources/maps/map05", true),
        TestMap("src/test/resources/maps/map06", true),
        TestMap("src/test/resources/maps/map07", true),
        TestMap("src/test/resources/maps/map08", true),
        TestMap("src/test/resources/maps/map09", true),
        TestMap("src/test/resources/maps/map10", true),
        TestMap("src/test/resources/maps/map01", false, listOf(Pair(2, 0))),
        TestMap("src/test/resources/maps/map01", false, blueBlockPositions = listOf(Pair(2, 0))),
    )
}

package navigation

import TestMap
import TestMaps
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.io.File
import java.io.FileNotFoundException
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PathfinderTest {

    fun mapProvider(): Stream<Arguments> = TestMaps.maps.map { map -> Arguments.of(map) }.stream()

    @ParameterizedTest
    @MethodSource("mapProvider")
    fun encounterBlockOnCurrentPosition(map: TestMap) {
        val pathfinderMazeReaderField = Pathfinder::class.java.getDeclaredField("mazeReader")
        pathfinderMazeReaderField.isAccessible = true

        if (!File(map.filePath).exists()) {
            throw FileNotFoundException(map.filePath)
        }
        println(map)
        val pathfinder = Pathfinder(map.filePath)

        map.redBlockPositions.forEach {
            (pathfinderMazeReaderField.get(pathfinder) as MazeReader).getNode(
                it.first,
                it.second
            )?.redBlock = true
        }
        map.blueBlockPositions.forEach {
            (pathfinderMazeReaderField.get(pathfinder) as MazeReader).getNode(
                it.first,
                it.second
            )?.blueBlock = true
        }
        val solved = when (pathfinder.aStar()) {
            Double.MAX_VALUE -> false
            else -> true
        }

        assertEquals(solved, map.solvable)
    }
}

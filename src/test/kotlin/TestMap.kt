data class TestMap(
    val filePath: String,
    val solvable: Boolean,
    val redBlockPositions: List<Pair<Int, Int>> = listOf(),
    val blueBlockPositions: List<Pair<Int, Int>> = listOf()
)
